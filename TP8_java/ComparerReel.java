import java.util.Comparator;

public class ComparerReel implements Comparator<Complexe>{
    @Override
    public int compare(Complexe reel1, Complexe reel2){
        return Double.compare(reel1.getPartieReel(), reel2.getPartieReel());
    }
}