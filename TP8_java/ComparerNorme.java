import java.util.Comparator;

public class ComparerNorme implements Comparator<Complexe>{
    
    @Override
    public int compare(Complexe comp1, Complexe comp2){
        double norme1 = Math.pow(comp1.getPartieReel(),2)+Math.pow(comp1.getPartieImaginaire(),2);
        double norme2 = Math.pow(comp2.getPartieReel(),2)+Math.pow(comp2.getPartieImaginaire(),2);
        return Double.compare(norme1, norme2);
    }




}