import java.util.Collections;
import java.util.ArrayList;
import java.util.List;


public class BibPersonne{
    public static List<Personne> trierLaListe(List<Personne> listePasTrier){
        List<Personne> listeTrier = new ArrayList<>(listePasTrier);
        Collections.sort(listeTrier, new ComparatorPersonne());
        return listeTrier;
    }
    public static int minEcartAge(List<Personne> listePasTrier) throws PasDEcartMin{
        if (listePasTrier.size() < 2){
            throw new PasDEcartMin();
        }
        List<Personne> liste = trierLaListe(listePasTrier);
        int ecartMin = 1000;
        for (int i = 1; i < liste.size(); i++){
            if((liste.get(i).getAge() - liste.get(i-1).getAge()) < ecartMin){
                ecartMin = liste.get(i).getAge() - liste.get(i-1).getAge();
            }
        }
        return ecartMin;
    }
}