import java.util.Comparator;
public class ComparatorComplexe implements Comparator<Complexe>{
    @Override
    public int compare(Complexe d1, Complexe d2){
        return Double.compare(d1.getPartieReel(), d2.getPartieReel());
    }

}
