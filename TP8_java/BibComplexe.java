import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class BibComplexe {
    
    public static List<Complexe> trierParNormeCroissante(List<Complexe> listeComp){
        Comparator<Complexe> comparatorC = new ComparerNorme();
        List<Complexe> listeComp2 = new ArrayList<>(listeComp);
        Collections.sort(listeComp2, comparatorC);
        return listeComp2;
    }

    public static List<Complexe> trierParNormeDecroissante(List<Complexe> listeComp){
        Comparator<Complexe> comparatorC = new ComparerNorme();
        List<Complexe> listeComp2 = new ArrayList<>(listeComp);
        Collections.sort(listeComp2, comparatorC.reversed());
        return listeComp2;
    }

    public static List<Complexe> trierReelCroissant(List<Complexe> listeComp){
        Comparator<Complexe> comparatorC = new ComparerReel();
        List<Complexe> listeComp2 = new ArrayList<>(listeComp);
        Collections.sort(listeComp2, comparatorC);
        return listeComp2;
        }
    
    public static Complexe getNormePlusGrande(List<Complexe> listeComp){
        Comparator<Complexe> comparatorC = new ComparerNorme();
        List<Complexe> listeComp2 = new ArrayList<>(listeComp);
        Collections.sort(listeComp2, comparatorC.reversed());
        return listeComp2.get(0);
    }


    public static boolean complexePlusPetit(List<Complexe> listeComp, Comparator<Complexe> comparateur, Complexe comp){
        for (Complexe comp2 : listeComp){
            if (comparateur.compare(comp, comp2) >= 0){
                return false;
            }
        }
        return true;
    }

    public static boolean complexePlusPetit(List<Complexe> listeComp, Comparator<Complexe> comparateur, Complexe comp, Complexe comp2){
        for (Complexe comp3 : listeComp){
            if (comparateur.compare(comp3, comp) < 0 || comparateur.compare(comp3, comp2) > 0){
                return false;
            }
        }
        return true;
    }








    
   
    




    }

// Écrivez une classe BibComplexes contenant une méthode prenant en argument une List<Complexe>, un
//Comparator et un Complexe et renvoyant vrai si ce Complexe est plus petit que tous les complexes de la liste
//et faux sinon. Testez votre code avec les différents comparateurs écrits précédemment.