public class Complexe {
    private double reel;
    private double imaginaire;
    public Complexe(double reel, double imaginaire)
    {
        this.reel = reel;
        this.imaginaire = imaginaire;
    }
    public double getPartieReel(){
        return this.reel;
    }
    public double getPartieImaginaire(){
        return this.imaginaire;
    }
    public double getNorme(){
        return Math.pow(getPartieReel(), 2)+Math.pow(getPartieImaginaire(), 2);
    }
}