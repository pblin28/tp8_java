import java.util.Comparator;
public class ComparatorPersonne implements Comparator<Personne>{
    @Override
    public int compare(Personne personne1, Personne personne2){
        return personne1.getAge() - personne2.getAge();
    }
}