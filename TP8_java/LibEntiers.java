import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Collections;

public class LibEntiers{
    

    public boolean sommeNulle(List<Integer> liste){
        Set<Integer> ensemble = new HashSet<>();
        
        for (int i : liste){
            if(ensemble.contains(-i)){
                return true;
            }
            ensemble.add(i);
        }
        return false;
    }

    public Integer plusPetiteDiff(List<Integer> liste){
        if(liste.size()<2){
            return null;
        }
        Collections.sort(liste);
        int minimumDiff = Collections.max(liste);
    
        for(int i = 1; i < liste.size(); i++){
            int diff = liste.get(i) - liste.get(i-1);
            
            if(diff > 0 && diff < minimumDiff){
                minimumDiff = diff;
            }
        
        
        }
    
        return minimumDiff;
    
    }

}