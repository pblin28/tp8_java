public class Personne{


    private int age;
    private String nom;

    public Personne(int age, String nom){
        this.nom = nom;
        this.age = age;
    }

    public String getNom(){
        return this.nom;
    }
    public int getAge(){
        return this.age;
    }

    @Override
    public String toString(){
        return this.getNom() + "a" + getAge();
    }
}